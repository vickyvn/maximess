
<div class="posts index">
    <h2><?php echo __('Posts'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            
            <th><?php echo $this->Paginator->sort('title'); ?></th>
            <th><?php echo $this->Paginator->sort('body'); ?></th>
            <th class="actions"><?php echo __('Like'); ?></th>
            
        </tr>
        </thead>
        <tbody>
        <?php foreach ($posts as $post): ?>
            <tr>
                
                <td><?php echo h($post['Post']['title']); ?>&nbsp;</td>
                <td><?php echo h($post['Post']['body']); ?>&nbsp;</td>
                <td><a href="">Like</a></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Login'), array('controller' => 'Users', 'action' => 'login')); ?></li>
    </ul>
</div>
