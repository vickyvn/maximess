<?php
/**
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Pages
 * @since         CakePHP(tm) v 0.10.0.1076
 */

if (!Configure::read('debug')):
	throw new NotFoundException();
endif;

App::uses('Debugger', 'Utility');
?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<form action="">
		<input type="email" name="email">
		<input type="password" name="password">
		<button>submit</button>
	</form>
	
</body>
</html>